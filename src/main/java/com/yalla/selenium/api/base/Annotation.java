package com.yalla.selenium.api.base;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import report.Daylibrary;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public  class Annotation extends SeleniumBase {
	
	@DataProvider(name="fetchData")
	public Object[][] fetchData() throws IOException{
		return Daylibrary.ReadExcel(excelfilename);
	
	}
    @Parameters({"url","username","password"})
	@BeforeMethod(groups= "any")
  public void beforeMethod(String url,String uname12,String pwd) {
	  startApp("Chrome",url);
	
		//* Login page User name and Password
	  
	  WebElement uname = locateElement("id","username");
	  clearAndType(uname, uname12);
	  
	  WebElement password = locateElement("id","password");
	  clearAndType(password, pwd);
	  
	  
	  WebElement submitbutton = locateElement("class","decorativeSubmit");
	  click(submitbutton);
	  
	  WebElement CRMlink = locateElement("link","CRM/SFA");
	  click(CRMlink);
	  
	  
	  
		//driver.manage().window().maximize();
		
		
		
		/*WebElement email = locateElement("id", "email");
		clearAndType(email, "9840557681");
		WebElement pwd = locateElement("id","pass");
		clearAndType(pwd, "Sriramgokul@123");
		WebElement login = locateElement("xpath","//input[@value='Log In']");
		login.click();	*/
  }

  @AfterMethod
  public void afterMethod() {
	 // close();
  }

  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

  @BeforeSuite
  public void beforeSuite() {
  }

  @AfterSuite
  public void afterSuite() {
  }

}
