package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotation;

public class face1 extends Annotation

{
	

	@BeforeClass
	public void SetData()
	{
		testcaseName="Facebook";
		testcaseDec="Testing a facebook application";
		author= "sriram";
		category="smoke";
	}
	
@Test
public void facebook() 
{	
//SearchTextbox and passing the value Test Leaf
WebElement Searchtxt = locateElement("xpath","//input[@placeholder='Search']");
clearAndType(Searchtxt,"TestLeaf");
// Click the Search icon next to the Search text
WebElement searchtest = locateElement("xpath","//button[@data-testid='facebar_search_button']");
click(searchtest);
//Select the Places and verify the test leaf is displayed
WebElement Placesicon = locateElement("xpath","//div[text()='Places']");
click(Placesicon);
WebElement Testleaflink = locateElement("link","TestLeaf");
verifyExactText(Testleaflink, "TestLeaf");
System.out.println(Testleaflink);
//capture  the text of the Like button
WebElement likebutton = locateElement("xpath","//button[@data-testid='search_like_button_test_id']");
click(likebutton);
WebElement alrlike = locateElement("xpath","//button[@class='likedButton _4jy0 _4jy3 _517h _51sy _42ft']");
getElementText(alrlike);
if(alrlike.equals("Liked"))
{
	System.out.println("Says the like button is getting already liked");
}
else
{
	System.out.println("the element is not clicked in the Liked button");
}
WebElement linktext = locateElement("link","TestLeaf");
click(linktext);
String testleaftitle = driver.getTitle();
verifyTitle(testleaftitle);
System.out.println(testleaftitle);
WebElement numberofpages = locateElement("xpath","//div[contains(text(),'people like this')]");
getElementText(numberofpages);
System.out.println(numberofpages);
}

}

