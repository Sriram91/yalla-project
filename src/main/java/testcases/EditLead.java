package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotation;

public class EditLead extends Annotation{
	

	@BeforeTest(groups="sanity")
	public void SetData()
	{
		testcaseName="EditLead";
		testcaseDec="Update a lead";
		author= "sriram";
		category="sanity";
		excelfilename = "EditLead";
	}	

	@Test(groups="sanity", dataProvider="fetchData")
		public void editLead(String UpdatedName)
		{
	
	/*WebElement clickele = locateElement("link","Leads");
	click(clickele);
	
	WebElement linknum = locateElement("link","10002");
	click(linknum);		
					// Edit the lead
					WebElement eleEditelement = locateElement("link","Edit");
					click(eleEditelement);
					//Update the text
					WebElement eleUpdate = locateElement("id","updateLeadForm_companyName");
					String cmpName=eleUpdate.getText();
					clearAndType(eleUpdate,UpdatedName);
					//Click Update button
					WebElement eleupdatebutton = locateElement("xpath","//input[@name='submitButton']");
					click(eleupdatebutton);
					// updated text using get text
					WebElement eleupdateresults = locateElement("id","viewLead_companyName_sp");
					String modifiedresult = eleupdateresults.getText();
					System.out.println(modifiedresult);
					// company name is modified or not
					
					if(cmpName.equals(modifiedresult))
					{
						System.out.println("Company Name is not modified successfully");
					}
					else
					{
						System.out.println("Company Name is modified successfully");
					}*/

		
		WebElement clickele = locateElement("link","Leads");
		click(clickele);
		
		WebElement linknum = locateElement("link","10020");
		click(linknum);		
		// Edit the lead
		WebElement eleEditelement = locateElement("link","Edit");
		click(eleEditelement);
		//Update the text
		WebElement eleUpdate = locateElement("id","updateLeadForm_companyName");
						getElementText(eleUpdate);
						clearAndType(eleUpdate,UpdatedName);
						//Click Update button
						WebElement eleupdatebutton = locateElement("xpath","//input[@name='submitButton']");
						click(eleupdatebutton);
						// updated text using get text
						WebElement eleupdateresults = locateElement("id","viewLead_companyName_sp");
						getElementText(eleupdateresults);
						// company name is modified or not
						
						if(eleUpdate.equals(eleupdateresults))
						{
							System.out.println("Company Name is not modified successfully");
						}
						else
						{
							System.out.println("Company Name is modified successfully");
						}

		}


	}


