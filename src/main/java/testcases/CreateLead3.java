package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotation;


		
public class CreateLead3 extends Annotation
{
			@BeforeTest(groups="smoke")
			public void setData() {
				testcaseName="CreateLead";
				testcaseDec="create a new lead";
				author= "sriram";
				category="smoke";
				excelfilename = "createLead";
			}

			
				@Test(groups="smoke", dataProvider="fetchData")
				public void createLead(String cName, String fName, String lName)

				{
					// Click Leads tab
					WebElement eleLeadstab = locateElement("link","Leads");
					click(eleLeadstab);
					// Click Create Lead Section
					WebElement eleCreatleadLink = locateElement("link", "Create Lead");
					click(eleCreatleadLink);
					// Enter Company Name
					WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
					clearAndType(eleCompanyName, cName);
					// Enter First Name
					WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
					clearAndType(eleFirstName, fName);
					// Enter Last Name
					WebElement eleLastName = locateElement("id","createLeadForm_lastName");
					clearAndType(eleLastName, lName);
					// dropdown selection for Source field using index
					WebElement ele2 = locateElement("id", "createLeadForm_dataSourceId");
					selectDropDownUsingIndex(ele2,4);
					// dropdown selection for Industry field using attribute value
					WebElement ele3 = locateElement("id", "createLeadForm_industryEnumId");
					selectDropDownUsingValue(ele3,"IND_TELECOM");
					// dropdown selection for ownership field using visible text
					WebElement ele = locateElement("id", "createLeadForm_ownershipEnumId");
					selectDropDownUsingText(ele,"Corporation");
					// Creating a Lead using create lead button
					WebElement eleCreateLeadButton = locateElement("xpath","//input[@value='Create Lead']");
					eleCreateLeadButton.click();

				}
}
	


