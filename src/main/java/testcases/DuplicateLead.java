package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotation;

public class DuplicateLead extends Annotation
{
			@BeforeTest(groups="smoke")
			public void setData() {
				testcaseName="DuplicateLead";
				testcaseDec="create a duplicate lead";
				author= "sriram";
				category="smoke";
				excelfilename = "DuplicateLead";
			}

			
				@Test(groups="smoke", dataProvider="fetchData")
				public void duplicate(String mail)

				{
					//6. Click Leads link
					WebElement linkdata = locateElement("link","Leads");
					click(linkdata);
					//7. Click Find leads
					WebElement findleadsdata = locateElement("link","Find Leads");
					click(findleadsdata);
					//8. Click on Email
					WebElement emaillink = locateElement("xpath","(//span[@class='x-tab-strip-text '])[3]");
	                click(emaillink);
					//9. Enter Email
	                WebElement emaildata = locateElement("xpath","(//input[@class=' x-form-text x-form-field'])[28]");
	                clearAndType(emaildata, mail);	
					//10. Click find leads button
	                WebElement findata = locateElement("xpath","(//button[text()='Find Leads'])");
	                click(findata);
					//11.Capture name of First Resulting lead
					String capturetext = driver.findElementByXPath("(//a[text()='TestLeaf'])[1]").getText();
					System.out.println(capturetext);
					//12. Capture name of First Resulting lead
					WebElement firstresults = locateElement("xpath","(//a[text()='TestLeaf'])[1]");
					click(firstresults);
					//13. Click Duplicate Lead
					WebElement dupdata = locateElement("link","Duplicate Lead");
					click(dupdata);
					//14. Verify the title as 'Duplicate Lead'
					String test23 = driver.getTitle();
					System.out.println(test23);
					//15. Click Create Lead
					WebElement createlead = locateElement("class","smallSubmit");
					click(createlead);
					/*16. Confirm the duplicated lead name is same as captured name
					String duplicatename = driver.findElementByXPath("//span[starts-with(text(),'Te')]").getText();
					System.out.println(duplicatename);
					if(driver.findElementByXPath("//span[starts-with(text(),'Te')]").equals(duplicatename))
							{
						System.out.println("Duplicate lead should be capture with the expected results");
							}
					else
					{
						System.out.println("Duplicate lead does not matched with the expected results");
					}*/
					//17. Close the browser (Do not log out)
					
					driver.close();
					
	}

}
